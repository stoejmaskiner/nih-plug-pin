# NIH Plug Pin

This is a re-export of [NIH-Plug]() to pin its version number. No other changes are made.

You should alias this as `nih_plug`, otherwise things like derive macros wont work.
